/*
 * soh-awtls.h
 *
 *  Created on: Sep 29, 2021
 *      Author: Admin
 */

#ifndef SOH_AWTLS_H_
#define SOH_AWTLS_H_


#define sigma_X0_square    0.001f
#define sigma_Y0_square    0.00001f
#define sigma_X0 		   0.01f
#define sigma_Y0           0.001f
#define K 				   10
#define Q_nominal   	   3.448
#define eta       1


typedef struct soh_awtls_t soh_awtls;

struct soh_awtls_t{
    float measureX; // delta soc = z[k2] - z[k1]
    float measureY; // accumlated I*deltat
    float sigma_X;
    float sigma_Y;
    float C1; 
    float C2;
    float C3;
    float C4;
    float C5;
    float C6;
    float A,B,C,D,E;
    float root[4];
    float result[4];
    float root_min;
    float Q_hat;
    float Q_nom;
    float Sigma_Q;
};

typedef struct complex_t complex;
struct complex_t{
	float real;
	float image;
};

void init_soh_awtls(soh_awtls* p_data);
void iter_soh_awtls(soh_awtls* p_data);

#endif /* SOH_AWTLS_H_ */
